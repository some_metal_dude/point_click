﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FPMouseLook : MonoBehaviour
{
    [SerializeField] Transform playerTransform;
    [SerializeField] Vector3 mousePosition;
    [SerializeField] public float rotationSpeed = 8f;

    public static bool RotationEnabled = true;
    [SerializeField] [Range(3f, 0.5f)] float TurningEdgeLeft = 0.2f;
    [SerializeField] [Range(7f, 9.5f)] float TurningEdgeRight = 0.8f;
    [SerializeField] [Range(3f, 0.5f)] float TurningEdgeDown = 0.2f;
    [SerializeField] [Range(7f, 9.5f)] float TurningEdgeUp = 0.8f;
    bool canRotateY = true;

    Ray ray;
    RaycastHit hit;

    float mouseSensitivity = 100f;
    float xRotation;

    void Start()
    {
        Cursor.lockState = CursorLockMode.Confined;
        Cursor.visible = true;
    }

    void Update()
    {
        // Convert mouse input to viewport
        mousePosition = Camera.main.ScreenToViewportPoint(Input.mousePosition);

        if (RotationEnabled)
        {
            // Rotates on x axis
            if (mousePosition.x > (TurningEdgeRight * 0.1f))
            {
                playerTransform.Rotate(Vector3.up * rotationSpeed * Time.deltaTime);
            }
            else if (mousePosition.x < (TurningEdgeLeft * 0.1))
                playerTransform.Rotate(Vector3.down * rotationSpeed * Time.deltaTime);

            // rotates on y axis
            if (mousePosition.y > (TurningEdgeUp * 0.1) && canRotateY)
            {
                playerTransform.Rotate(Vector3.left * rotationSpeed * Time.deltaTime);
            }
            else if (mousePosition.y < (TurningEdgeDown * 0.1) && canRotateY)
                playerTransform.Rotate(Vector3.right * rotationSpeed * Time.deltaTime);

            if (transform.rotation.y > 90f || transform.localRotation.y < -90f) canRotateY = false;
            else canRotateY = true;
        }
    }

    void FirstPersonMouseLook()
    {
        // Looking stuff
        float MouseX = Input.GetAxis("Mouse X") * mouseSensitivity * Time.deltaTime;
        float MouseY = Input.GetAxis("Mouse Y") * mouseSensitivity * Time.deltaTime;

        xRotation -= MouseY;
        xRotation = Mathf.Clamp(xRotation, -90f, 90f);

        transform.localRotation = Quaternion.Euler(xRotation, 0f, 0f);
        playerTransform.Rotate(Vector3.up * MouseX);
    }
}
