﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FastTravel : MonoBehaviour
{
    [SerializeField] public GameObject Subject;
    [SerializeField] public Transform Location;

    public void TeleportToLocation()
    {
        Subject.transform.position = Location.transform.position;
        Debug.Log("Transporting");
    }

    public void MoveToLocation(float speed)
    {
        while (!TravelComplete())
        {
            Subject.transform.position = Vector3.MoveTowards(Subject.transform.position, Location.position, speed * Time.deltaTime);
            Debug.Log("Transporting");
        }
    }

    public bool TravelComplete()
    {
        return (Subject.transform.position == Location.transform.position);
    }
}
