﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using TMPro;

public class PlayerMoveTowards : MonoBehaviour
{
    [SerializeField] float MovementSpeed;
    public static bool MoveEnabled = true;
    Ray ray;
    RaycastHit hit;
    bool running = false;

    NavMeshAgent nav;
    //[SerializeField] private Text LocationText;
    [SerializeField] TextMeshProUGUI LocationText;

    void Start()
    {
        nav = GetComponent<NavMeshAgent>();
    }

    void Update()
    {
        if (MoveEnabled)
        {
            // Raycast stuff
            ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out hit))
            {
                if (Input.GetMouseButtonDown(0))
                {
                    print(hit.collider.name);
                    //Movement(transform.position, hit.transform.position, 3f);
                    //Vector3.Lerp(transform.position, hit.point, MovementSpeed * Time.deltaTime);
                    nav.destination = hit.point;
                }
            }
            if (nav.remainingDistance <= nav.stoppingDistance)
            {
                running = false;
                if (!running && LocationText != null)
                {
                    StopCoroutine("AnimateText");
                    LocationText.text = "Now at: " + transform.position.x.ToString("F0") + " , " + transform.position.z.ToString("F0");
                }
            }
            else
            {
                running = true;
                if (LocationText != null)
                {
                    StartCoroutine("AnimateText");
                }
            }
        }
    }

    void Movement(Vector3 startPosition, Vector3 target, float time)
    {
        float t = 0.0f;
        while (time <= 1.0f)
        {
            t += Time.deltaTime / time;
            transform.position = Vector3.Lerp(startPosition, target, Mathf.SmoothStep(0, 1, t));
        }
    }

    IEnumerator AnimateText()
    {
        LocationText.text = "Relocating.";
        yield return new WaitForSeconds(1f);
        LocationText.text = "Relocating..";
        yield return new WaitForSeconds(1f);
        LocationText.text = "Relocating...";
        yield return new WaitForSeconds(1f);
        StartCoroutine("AnimateText");
    }
}
