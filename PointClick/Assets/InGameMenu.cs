﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InGameMenu : MonoBehaviour
{
    [SerializeField] GameObject Menu;
    [SerializeField] bool FreezeTime = false;
    public static bool MenuOpen;

    void Update()
    {
        if (Input.GetButtonDown("Jump"))
        {
            if (MenuOpen)
            {
                ResumeGame();
                FPMouseLook.RotationEnabled = true;
            }
            else
            {
                PauseGame();
                FPMouseLook.RotationEnabled = false;
            }
        }
    }

    public void ResumeGame()
    {
        Menu.SetActive(false);
        if (FreezeTime) Time.timeScale = 1f;
        MenuOpen = false;
    }

    void PauseGame()
    {
        Menu.SetActive(true);
        if (FreezeTime) Time.timeScale = 0f;
        MenuOpen = true;
    }
}
